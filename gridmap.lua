gridmap = {}

gridmap.data = {
   {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
   {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
   {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
   {1, 0, 1, 1, 0, 1, 1, 1, 1, 1},
   {1, 0, 1, 1, 0, 1, 1, 1, 1, 1},
   {1, 0, 0, 0, 0, 1, 1, 1, 1, 1},
   {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
   {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
   {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
   {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
}

function gridmap:check(x, y)
   if x <= 0 or y <= 0 then return false end
   if self.data[y][x] == 1 then return false end
   return true
end

function gridmap:new(o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

return gridmap