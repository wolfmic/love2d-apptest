local class = require 'class'
local math = require 'math'

local vec2 = class:new()
vec2.x = 0
vec2.y = 0

function vec2.distance(a, b)
    return math.sqrt(((a.x - b.x)+(a.y - b.y))*((a.x - b.x)+(a.y - b.y)))
end

function vec2:print()
    print(self, self.x, self.y)
end

local element = class:new()
element.visited = false
element.next = nil

data = {
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 0, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 0, 1, 1, 1, 1, 1},
    {1, 0, 0, 0, 0, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
}

